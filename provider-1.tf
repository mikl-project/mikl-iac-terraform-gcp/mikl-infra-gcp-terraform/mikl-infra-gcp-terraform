#this is the main file which can combine terraform cloud+terraform+google cloud

terraform {
   backend "remote" {
           hostname = "app.terraform.io"
           organization = "Mcs-UK"
           workspaces {
                name = "mkl-dev"  
          }
      }
}
provider "google" {
impersonate_service_account = "terraform-gcp-original@mkl-dev.iam.gserviceaccount.com"
project = "mkl-dev"
# credentials = "/opt/terraform-mirakl/mirkl-test/gcp-sa-key.json"
}
