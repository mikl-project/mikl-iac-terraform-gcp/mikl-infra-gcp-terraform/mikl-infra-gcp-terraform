module "impersonate-sa-iam" {

    source  = "app.terraform.io/Mcys-UK/impersonate-sa-iam/google"
    version = "1.0.0"
    project = "mikl-dev"
    account_id = "terraform-gcp-original"
    display_name = "terraform-gcp-original"
    type = "serviceAccount"
    role = ["roles/cloudquotas.admin", "roles/compute.networkAdmin", "roles/compute.securityAdmin", "roles/storage.admin", "roles/pubsub.admin", "roles/compute.instanceAdmin", "roles/cloudsql.admin"]
    
}
